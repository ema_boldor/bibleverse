$(document).ready(function () {
    $.ajax({
      url: "https://getbible.net/json",
      dataType: "jsonp",
      data: `passage=${getRandomBook()}`,
      contentType: "application/json; charset=utf-8",
      jsonp: "getbible",
      success: function (json) {
        var book = json.book_name;
        var chapter_nr =
          Math.floor(Math.random() * getObjectNumber(json.book)) + 1;
        var verse_nr;
        var verse_text;
        $.each(json.book, function (index, value) {
          if (parseInt(index) == chapter_nr) {
            verse_nr =
              Math.floor(Math.random() * getObjectNumber(value.chapter)) + 1;
            $.each(value.chapter, function (index, value) {
              if (parseInt(index) == verse_nr) {
                verse_text = value.verse;
              }
            });
          }
        });
        $("#verse_address").text(`${book} ${chapter_nr}:${verse_nr}`);
        $("#verse_text").text(verse_text);
      },
      error: function () {
        $("#verse_text").text("No scripture was returned, please try again!");
      }
    });
  });
  
  function getRandomBook() {
    return [
        "Genesis",
        "Exodus",
        "Leviticus",
        "Numbers",
        "Deuteronomy",
        "Joshua",
        "Judges",
        "Ruth",
        "1 Samuel",
        "2 Samuel",
        "1 Kings",
        "2 Kings",
        "1 Chronicles",
        "2 Chronicles",
        "Ezra",
        "Nehemiah",
        "Esther",
        "Job",
        "Psalm",
        "Proverbs",
        "Ecclesiastes",
        "Song of Solomon",
        "Isaiah",
        "Jeremiah",
        "Lamentations",
        "Ezekiel",
        "Daniel",
        "Hosea",
        "Joel",
        "Amos",
        "Obadiah",
        "Jonah",
        "Micah",
        "Nahum",
        "Habakkuk",
        "Zephaniah",
        "Haggai",
        "Zechariah",
        "Malachi",
        "Matthew",
        "Mark",
        "Luke",
        "John",
        "Acts",
        "Romans",
        "1 Corinthians",
        "2 Corinthians",
        "Galatians",
        "Ephesians",
        "Philippians",
        "Colossians",
        "1 Thessalonians",
        "2 Thessalonians",
        "1 Timothy",
        "2 Timothy",
        "Titus",
        "Philemon",
        "Hebrews",
        "James",
        "1 Peter",
        "2 Peter",
        "1 John",
        "2 John",
        "3 John",
        "Jude",
        "Revelation"
    ][Math.floor(Math.random() * 66) + 1];
  }
  
  function getObjectNumber(object) {
    var length = 0;
    for (var key in object) {
      if (object.hasOwnProperty(key)) {
        ++length;
      }
    }
    return length;
  }